#include <Windows.h>
#include <MmDDK.h>

#pragma comment (lib, "Winmm.lib")
#pragma comment (lib, "Kernel32.lib")
#pragma comment (lib, "Shell32.lib")

#define ASSERT_MSG(msg) ((hStd != INVALID_HANDLE_VALUE) && WriteConsoleW(hStd, msg, lstrlenW(msg), &lResult, NULL))
#define DRIVER_NAME TEXT("wavemapper")

// errors!
#define ARGS_ERR TEXT("Parameter error! Pass file path name to app!\n")
#define OPEN_DRV_ERR TEXT("Cannot open required adpcm driver from system drivers!\n")
#define MOD_DRV_ERR TEXT("Cannot get adpcm driver module!\n")
#define PLAY_ERR TEXT("Cannot play sound!\n")
#define DRV_CLOSE_ERR TEXT("Cannot close sound driver!\n")
#define ARGS_FREE_ERR TEXT("Cannot free argument memory!\n")

#if defined _DEBUG || defined DEBUG || defined DBG
#ifndef DEBUG_MODE
#define DEBUG_MODE
#endif
#define SAMPLE_FILE_PATH TEXT("R:\\Shared\\sample_ms_adpcm.wav")
#else
#define SAMPLE_FILE_PATH TEXT("")
#endif

void EntryPoint(void) {
	HDRVR hDrvR;
	int nArgs = 0;
	HANDLE hStd = GetStdHandle(STD_OUTPUT_HANDLE);
	LRESULT lResult = ERROR_SUCCESS;
	BOOL bAssert = FALSE, bPlay = FALSE;
	LPWSTR lpCmdLine = GetCommandLineW(), *lpArgs = CommandLineToArgvW(lpCmdLine, &nArgs);
	if (nArgs < 2) {
		bAssert = ASSERT_MSG(ARGS_ERR);
		goto __noargs;
	}
	hDrvR = OpenDriver(DRIVER_NAME, DRIVERS_SECTION, NULL);
	HMODULE hModule;
	if (!hDrvR) {
		bAssert = ASSERT_MSG(OPEN_DRV_ERR);
		goto __nodrv;
	}
	hModule = DrvGetModuleHandle(hDrvR);
	if (!hModule) {
		bAssert = ASSERT_MSG(MOD_DRV_ERR);
		goto __nodrvmod;
	}
	bPlay = PlaySoundW(lpArgs[1], hModule, SND_SYNC | SND_FILENAME); // must be sync in all cases!
#ifdef DEBUG_MODE
	if (!bPlay) {
		bAssert = ASSERT_MSG(PLAY_ERR);
		bAssert = ASSERT_MSG(TEXT("Retrying...\n"));
		hModule = GetDriverModuleHandle(hDrvR);
		bPlay = PlaySoundW(SAMPLE_FILE_PATH, hModule, SND_SYNC | SND_FILENAME);
		if (!bPlay && !(bPlay = PlaySoundW(lpArgs[1], hModule, SND_SYNC | SND_FILENAME)))
			bAssert = ASSERT_MSG(PLAY_ERR);
	}
#else
	if (!bPlay)
		bAssert = ASSERT_MSG(PLAY_ERR);
#endif
__nodrvmod:
	lResult = CloseDriver(hDrvR, NULL, NULL);
	if (!lResult)
		bAssert = ASSERT_MSG(DRV_CLOSE_ERR);
__nodrv:
__noargs:
	lpArgs = LocalFree(lpArgs);
	if(lpArgs)
		bAssert = ASSERT_MSG(ARGS_FREE_ERR);

	ExitProcess(lResult);
}